//
//  SNTPListener.m
//  SNTPClient
//
//  Created by Yury on 07/08/15.
//  Copyright © 2015 Yury. All rights reserved.
//

#import "SNTPListener.h"
#import "SntpClient.h"

#define LogT NSLog(@"%s time interval = %f (%f)", __PRETTY_FUNCTION__, _sntpClient.timeIntervalSince1970, \
[[NSDate new] timeIntervalSince1970] - _sntpClient.timeIntervalSince1970)

@interface SNTPListener()<SntpClientListener>

@end

@implementation SNTPListener
{
    SntpClient *_sntpClient;
}

- (void)start
{
    _sntpClient = [SntpClient getSharedInstance];
    [_sntpClient resetSntpDrift:self];
    LogT;
    
    [self timer];
}

- (void)timer
{
    static int resetCounter = 0;
    resetCounter++;
    LogT;
    if(resetCounter % 10 == 0)
    {
        NSLog(@"SntpClient reset");
        [_sntpClient resetSntpDrift:self];
    }
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(2 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [self timer];
    });
}
#pragma mark - delegate
-(void)sntpResetResult:(bool)success
{
    LogT;
}

@end
