//
//  SNTPListener.h
//  SNTPClient
//
//  Created by Yury on 07/08/15.
//  Copyright © 2015 Yury. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SNTPListener : NSObject

- (void)start;

@end
