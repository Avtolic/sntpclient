//
//  main.m
//  SNTPClient
//
//  Created by Yury on 07/08/15.
//  Copyright © 2015 Yury. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
