//
//  GFSntpClient.h
//  Grand Feux Loto Quebec 2014
//
//  Created by Jerome Tetreault on 2014-06-18.
//  Copyright (c) 2014 Grand Feux de Loto-Québec. All rights reserved.
//

#import "AsyncUdpSocket.h"

@protocol SntpClientListener <NSObject>
-(void)sntpResetResult:(bool)success;
@end

@interface SntpClient : NSObject <AsyncUdpSocketDelegate>
+ (id)getSharedInstance;
-(void)resetSntpDrift:(id<SntpClientListener>)_listener;
-(double)timeIntervalSince1970;
@end