//
//  GFSntpClient.m
//  Grand Feux Loto Quebec 2014
//
//  Created by Jerome Tetreault on 2014-06-18.
//  Copyright (c) 2014 Grand Feux de Loto-Québec. All rights reserved.
//

#import "SntpClient.h"
#import "AsyncUdpSocket.h"
#import <mach/mach_time.h>
#import "Config.h"

#define NTP_MODE_CLIENT 3
#define NTP_VERSION 3
#define TRANSMIT_TIME_OFFSET 40
#define REFERENCE_TIME_OFFSET 16
#define ORIGINATE_TIME_OFFSET 24
#define RECEIVE_TIME_OFFSET 32
#define OFFSET_1900_TO_1970 ((uint64_t)((365 * 70) + 17) * 24 * 60 * 60)

#define MIN_NTP_RESET_INTERVAL 0.0
#define MAX_NTP_REQUESTS 10
#define MAX_NTP_TIMEOUT 5
#define MAX_CORRELATION_MSEC 100.0
#define NUM_CORRELATION_SAMPLES 3

@implementation SntpClient {
    AsyncUdpSocket *udpSocket;
    double lastDrift;
    int timeoutCount;
    double lastResetTime;
    bool lastResetSuccess;
    NSMutableArray *driftArray;
    id<SntpClientListener> listener;
    
    uint64_t requestTicks;
    uint64_t requestTime;
}

- (id)init
{
    self = [super init];
    lastResetTime = 0.0;
    lastResetSuccess = false;
    lastDrift = 0.0;
    driftArray = [NSMutableArray new];

    return self;
}

+ (id)getSharedInstance {
    static SntpClient *sharedSntpClient = nil;
    @synchronized(self) {
        if (sharedSntpClient == nil)
            sharedSntpClient = [[self alloc] init];
    }
    
    return sharedSntpClient;
}

-(void)resetSntpDrift:(id<SntpClientListener>)_listener {
    listener = _listener;
    
    if ([[NSDate date] timeIntervalSince1970] - lastResetTime > MIN_NTP_RESET_INTERVAL || !lastResetSuccess) {
        lastResetTime = [[NSDate date] timeIntervalSince1970];

        // is it necessary for long term stability to create a new socket here...?
        udpSocket = [[AsyncUdpSocket alloc] initWithDelegate:self];
    
        lastDrift = 0.0;
        [driftArray removeAllObjects];
        timeoutCount = 0;
        
        [self requestSntpDriftAsynch];
    }
    else {
        [self callback:true];
    }
}

- (double)timeIntervalSince1970 {
    double systime =[[NSDate date] timeIntervalSince1970];
    
    return systime + ((double)lastDrift)/1000.0;
}

-(void)requestSntpDriftAsynch {
    // prevents reentry in udpSocket code on succesive calls (crashes if we don't do that)
    //sleep(0.1);
    
    requestTime = (uint64_t)([[NSDate date] timeIntervalSince1970] * 1000);
    requestTicks = mach_absolute_time() / 1000000;
    
    NSMutableData *data = [NSMutableData dataWithLength:48];
    uint8_t zeroByte = NTP_MODE_CLIENT | (NTP_VERSION << 3);
    [data replaceBytesInRange:NSMakeRange(0,1) withBytes:&zeroByte];
    
    writeTimeStamp(data, TRANSMIT_TIME_OFFSET, requestTime);
    
    [udpSocket sendData:data toHost:ntpServer port:123 withTimeout:1 tag:1];
    [udpSocket receiveWithTimeout:1 tag:1];
}

- (BOOL)onUdpSocket:(AsyncUdpSocket *)sock didReceiveData:(NSData *)data withTag:(long)tag fromHost:(NSString *)host port:(UInt16)port
{
    int64_t responseTicks = mach_absolute_time() / 1000000;
    int64_t responseTime = requestTime + (responseTicks - requestTicks);
    int64_t originateTime = readTimeStamp(data, ORIGINATE_TIME_OFFSET);
    int64_t receiveTime = readTimeStamp(data, RECEIVE_TIME_OFFSET);
    int64_t transmitTime = readTimeStamp(data, TRANSMIT_TIME_OFFSET);
    
    //int64_t roundTripTime = responseTicks - requestTicks - (transmitTime - receiveTime);
    lastDrift = (double)(((receiveTime - originateTime) + (transmitTime - responseTime))/2);

    NSLog(@"NTP Drift[%lu] : %f", (unsigned long)driftArray.count, lastDrift);
    
    [driftArray addObject:[NSNumber numberWithDouble:lastDrift]];
    
    if (driftArray.count >= NUM_CORRELATION_SAMPLES) {
        double sum = 0.0;
        double min, max;
        min = max = lastDrift;
        for (long j = driftArray.count - NUM_CORRELATION_SAMPLES; j < driftArray.count; j++) {
            NSNumber *nsnumber = driftArray[j];
            double number = nsnumber.doubleValue;
            sum += number;
            if (number < min)
                min = number;
            if (number > max)
                max = number;
        }
        
        lastDrift = sum / 3.0;
        
        NSLog(@"NTP[%lu] min = %f, max = %f, average = %f", (unsigned long)driftArray.count, min, max, sum/3.0);
        
        if (max-min < MAX_CORRELATION_MSEC) {
            NSLog(@"NTP Converged : %f", lastDrift);
            
            if (listener != nil) {
                [self callback:true];
            }
        }
        else if (driftArray.count < MAX_NTP_REQUESTS) {
            [self requestSntpDriftAsynch];
        }
        else {
            [self callback:false];
        }
    }
    else if (driftArray.count < MAX_NTP_REQUESTS) {
        [self requestSntpDriftAsynch];
    }
    else {
        [self callback:false];
    }
    
    return true;
}

- (void)onUdpSocket:(AsyncUdpSocket *)sock didNotReceiveDataWithTag:(long)tag dueToError:(NSError *)error {
    NSLog(@"NTP Timeout ***");
    
    timeoutCount++;
    
    if (timeoutCount < MAX_NTP_TIMEOUT) {
        [self requestSntpDriftAsynch];
    }
    else {
        if (listener != nil) {
            [self callback:false];
        }
    }
}

- (void)callback:(bool)success {
    lastResetSuccess = success;
    [listener sntpResetResult:success];
    listener = nil;
}

double random2 () {
    srandom((unsigned int)time(NULL));
    return random();
}

uint64_t readTimeStamp(NSData *data, int offset) {
    NSCAssert(data && (data.length >= offset + sizeof(uint64_t)), @"SNTPClent error - Incorrect data length!");

    uint32_t seconds, fraction;
    [data getBytes:&seconds range:NSMakeRange(offset, sizeof(uint32_t))];
    [data getBytes:&fraction range:NSMakeRange(offset + sizeof(uint32_t), sizeof(uint32_t))];
    
    // Convert numbers from big-endian
    seconds = CFSwapInt32BigToHost(seconds);
    fraction = CFSwapInt32BigToHost(fraction);
    
    return ((int64_t)seconds - OFFSET_1900_TO_1970) * 1000 + (int64_t) fraction * 1000 / (int64_t) 0x100000000;
}

void writeTimeStamp(NSMutableData *data, int offset, uint64_t time) {
    NSCAssert(data && (data.length >= offset + sizeof(uint64_t)), @"SNTPClent error - Incorrect data length!");
    
    uint32_t seconds = floor (time / 1000);
    uint64_t milliseconds = time - (uint64_t) seconds * 1000;
    seconds += OFFSET_1900_TO_1970;
    uint32_t fraction = round ((uint64_t)milliseconds * 0x100000000 / 1000);
    
    // Convert numbers to big-endian
    uint32_t seconds32 = CFSwapInt32HostToBig(seconds);
    uint32_t fraction32 = CFSwapInt32HostToBig(fraction);
    
    [data replaceBytesInRange:NSMakeRange(offset, sizeof(uint32_t)) withBytes:&seconds32];
    [data replaceBytesInRange:NSMakeRange(offset + sizeof(uint32_t), sizeof(uint32_t)) withBytes:&fraction32];
    // low order bits should be random data
    uint8_t randomNumber = random2() * 255.0;
    [data replaceBytesInRange:NSMakeRange(offset + sizeof(uint32_t) + 3, sizeof(uint8_t)) withBytes:&randomNumber];
}


@end
