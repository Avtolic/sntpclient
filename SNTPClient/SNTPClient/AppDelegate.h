//
//  AppDelegate.h
//  SNTPClient
//
//  Created by Yury on 07/08/15.
//  Copyright © 2015 Yury. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

